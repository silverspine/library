# Library App

Code test for Maniak

## Intallation

1. Clone the repo
2. Create a `.env` file to set the app config (database access and stuff like that). You can just copy the contents of `.env.example` and modify the required params.
2. Run `composer install` to install all the dependendencies.
3. Run `php artisan migrate` to run the migrations.
4. Run `php artisan db:seed` to run seed the database.

## Runing the code

1. The simple way is to run `php artisan serve` from the console to start the server.
2. Go to the address inditade in the response of the command, `http://127.0.0.1:8000` by default.
3. ENJOY!!!!!! =)

## Things to keep in mind

1. To lend a book, go to it's details, if it's available it'll show a button to "Lend" the book; if you click that button it'll display a modal with a list of users so you can select which user to lend the book to.
2. In a similar manner, to retrieve a book you have to go to the user profile, in there you'll see all the books that user has borrowed, from there you can select to return a book.
3. To filter a data table, the headers are the fiter fields, after you've added the desired filters just click the filter and refresh button. There is also a button to clear all the filters and reset the data table.