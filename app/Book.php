<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
	use SoftDeletes;

    protected $fillable = ['name', 'author', 'category_id', 'published_date'];

    protected $dates =['created_at', 'updated_at', 'deleted_at'];

    protected $casts = [
        'borrowed' => 'boolean',
    ];

    function getBorrowedAttribute(){
    	return $this->user;
    }

    function category() {
    	return $this->belongsTo('App\Category');
    }

    function user(){
    	return $this->belongsTo('App\User');
    }
}
