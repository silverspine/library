<?php

namespace App\Http\Controllers;

class PaginationHelper
{
    public static function getPaginationControls($index, $totalTabs)
    {
        $controls = '<a href="#" '.($index > 1 ? ('data-val="'.($index-1).'"') : 'data-val="1"  class="dissabled"' ).'>Previous</a>';
        $controls .= '<a href="#" data-val="1" '.($index <= 1 ? 'class="active"' : '' ).'>1</a>';

        if($index-2 > 1)
            $controls .= '<a href="#" data-val="'.($index-2).'" class="dissabled">&hellip;</a>';

        if($index-1 >= 2)
            $controls .= '<a href="#" data-val="'.($index-1).'">'.($index-1).'</a>';
        
        if($index != 1 && $index != $totalTabs)
            $controls .= '<a href="#" data-val="'.$index.'" class="active">'.$index.'</a>';

        if($index+1 <= $totalTabs-2)
            $controls .= '<a href="#" data-val="'.($index+1).'">'.($index+1).'</a>';

        if($index+2 < $totalTabs-1)
            $controls .= '<a href="#" data-val="'.($index+2).'" class="dissabled">&hellip;</a>';

        if($totalTabs > 1)
            $controls .= '<a href="#" data-val="'.$totalTabs.'" '.($index == $totalTabs ? 'class="active"' : '' ).'>'.$totalTabs.'</a>';

        $controls .= '<a href="#" '.($index < $totalTabs ? 'data-val="'.($index+1).'"' : 'data-val="'.$totalTabs.'"  class="dissabled"' ).'>Next</a>';

        return $controls;
    } 
}