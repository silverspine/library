<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Category;

class BookController extends Controller
{
    private $validatorRules = [
        'name' => 'required|min:3|max:255',
        'author' => 'required|min:3|max:255',
        'category_id' => 'required|exists:categories,id',
        'published_date' => 'required|date'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::select('name')->get();
        return view('books.index', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::select('id','name')->get();
        return view('books.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->validatorRules);
        $book = Book::create([
            'name' => $request->name,
            'author' => $request->author,
            'category_id' => $request->category_id,
            'published_date' => $request->published_date
            ]);
        if($book){
            return redirect()->route('books.show', ['book'=>$book->id]);
        }else{
            return redirect()->route('books.create', ['errors' => ['unespecified_error' => 'there was a problem storing the book']]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::find($id)->load('user', 'category');
        if($book)
            return view('books.show', ['book' => $book]);
        else
            return redirect()->route('books.index', ['error' => 'The specified book could not be found']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::select('id','name')->get();
        $book = Book::find($id);
        if($book)
            return view('books.edit', ['book' => $book, 'categories' => $categories]);
        else
            return redirect()->route('books.index', ['error' => 'The specified book could not be found']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->validatorRules);
        $book = Book::find($id);
        if($book){
            $book->name = $request->name;
            $book->author = $request->author;
            $book->category_id = $request->category_id;
            $book->published_date = $request->published_date;
            $book->save();
            return redirect()->route('books.show', ['book'=>$book->id]);
        }
        else
            return redirect()->route('books.index', ['error' => 'The specified book could not be found']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);
        if($book){
            if($book->delete())
                $json=['success' => true, 'msg' => 'the book was deleted'];
            else
                $json=['success' => flase, 'msg' => 'the book cold not be deleted'];
            return response()->json($json)->header('Content-Type', 'application/json');
        }
        else
            return response()->json(['success' => false, 'msg' => 'the specified book could not be found'])->header('Content-Type', 'application/json');
    }

    public function data(Request $request){
        $id = $request->has('id') ? $request->id : null;
        $name = $request->has('name') ? $request->name : null;
        $author = $request->has('author') ? $request->author : null;
        $category_name = $request->has('category') ? $request->category : null;
        $published_date = $request->has('published_date') ? $request->published_date : null;
        $user_id = $request->has('user_id') ? $request->user_id : null;
        $category_id_param = $request->has('category_id') ? $request->category_id : null;

        if(!$category_id_param){
            if($category_name)
                $category_id = Category::whereName($category_name)->pluck('id')->first();
            else
                $category_id = null;
        }else{
            $category_id = $category_id_param;
        }

        $books_query = Book::with('category');
        if($id)
            $books_query->whereId($id);
        if($name)
            $books_query->whereName($name);
        if($author)
            $books_query->whereAuthor($author);
        if($category_id)
            $books_query->whereCategoryId($category_id);
        if($published_date)
            $books_query->wherePublishedDate($published_date);
        if($user_id)
            $books_query->whereUserId($user_id);

        $limit = 10;
        $totalTabs = ceil($books_query->count()/$limit);

        $index = $request->has('index') ? ($request->index > $totalTabs ? $totalTabs : $request->index) : 1;
        $offset = ($index-1)*$limit;

        $books = $books_query->take($limit)->skip($offset)->get();

        $formatted = '';
        foreach ($books as $book) {
            $formatted .= '<tr>';
            $formatted .= ' <td>'.$book->id.'</td>';
            $formatted .= ' <td>'.$book->name.'</td>';
            $formatted .= ' <td>'.$book->author.'</td>';
            if (!$category_id_param)
                $formatted .= ' <td>'.$book->category->name.'</td>';
            $formatted .= ' <td>'.$book->published_date.'</td>';
            if(!$user_id)
                $formatted .= ' <td>'.($book->borrowed ? '<span class="glyphicon glyphicon-ok" aria-hidden="true" title="Borrowed"></span>' : '').'</td>';
            $formatted .= ' <td>';
            $formatted .= '     <a href="'.route('books.show',$book->id).'"><span class="glyphicon glyphicon-info-sign" aria-hidden="true" title="Info"></span></a>';
            if(!$user_id && !$category_id_param){
                $formatted .= '     <a href="'.route('books.edit',$book->id).'"><span class="glyphicon glyphicon-edit" aria-hidden="true" title="Edit"></span></a>';
                $formatted .= '     <a href="'.route('books.destroy',$book->id).'" class="destroy_button"><span class="glyphicon glyphicon-remove red" aria-hidden="true" title="Remove"></span></a>';
            }
            if($user_id)
                $formatted .= '     <a href="#" class="return_book" data-toggle="modal" data-target="#returnBookModal" data-url="'.route('books.retrieve',$book->id).'"><span class="glyphicon glyphicon-ok" aria-hidden="true" title="Return Book"></span></a>';
            $formatted .= ' </td>';
            $formatted .= '</tr>';
        }

        $controls = PaginationHelper::getPaginationControls($index, $totalTabs);

        return response()->json(['success' => true, 'data' => $formatted, 'controls' => $controls])->header('Content-Type', 'application/json');
    }

    public function borrow(Request $request, $id){
        $this->validate($request, ['user_id' => 'required|exists:users,id']);

        $book = Book::find($id);
        if($book){
            $book->user_id = $request->user_id;
            $book->save();
            return response()->json(['success' => false, 'msg' => 'the book was successfully retrieved'])->header('Content-Type', 'application/json');
        }else
            return response()->json(['success' => false, 'msg' => 'the specified book could not be found'])->header('Content-Type', 'application/json');
    }

    public function retrieve($id){
        $book = Book::find($id);
        if($book){
            $book->user_id = null;
            $book->save();
            return response()->json(['success' => false, 'msg' => 'the book was successfully retrieved'])->header('Content-Type', 'application/json');
        }else
            return response()->json(['success' => false, 'msg' => 'the specified book could not be found'])->header('Content-Type', 'application/json');
    }

    public function returnModal(Request $request){
        $rules = ['route' => 'required'];
        
        $this->validate($request, $rules);

        $view = view('books.modals.return', ['route' => $request->route]);

        return response()->json(['success' => true, 'payload' => view('books.modals.return', ['route' => $request->route])->render()]);
    }

    public function lendModal(Request $request){
        $rules = [
            'route' => 'required',
            'user_id' => 'required|exists:users,id',
        ];
        $this->validate($request, $rules);

        $view = view('layouts.modals.lend', ['route' => $request->route, 'user_id' => $request->user_id]);

        return response()->json(['success' => true, 'payload' => view('layouts.modals.lend', ['route' => $request->route, 'user_id' => $request->user_id])->render()]);
    }
}
