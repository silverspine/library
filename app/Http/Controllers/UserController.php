<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    private $validatorRules = [
        'name' => 'required|min:3|max:255',
        'email' => 'required|email|unique:users,email'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->validatorRules);
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email
            ]);
        if($user){
            return redirect()->route('users.show', ['user'=>$user->id]);
        }else{
            return redirect()->route('users.create', ['errors' => ['unespecified_error' => 'there was a problem storing the user']]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id)->load('books');
        if($user)
            return view('users.show', ['user' => $user]);
        else
            return redirect()->route('users.index', ['error' => 'The specified user could not be found']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        if($user)
            return view('users.edit', ['user' => $user]);
        else
            return redirect()->route('users.index',['error' => 'The specified user could not be found']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $new_rules = $this->validatorRules;
        $new_rules['email'] = 'required|email|unique:users,email,'.$user->id;
        $this->validate($request, $new_rules);
        if($user){
            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();
            return redirect()->route('users.show', ['user'=>$user->id]);
        }
        else
            return redirect()->route('users.index')->with(['error' => 'The specified user could not be found']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if($user){
            if($user->delete())
                $json=['success' => true, 'msg' => 'the user was deleted'];
            else
                $json=['success' => flase, 'msg' => 'the user cold not be deleted'];
            return response()->json($json)->header('Content-Type', 'application/json');
        }
        else
            return response()->json(['success' => false, 'msg' => 'the specified user could not be found'])->header('Content-Type', 'application/json');
    }

    public function data(Request $request){
        $id = $request->has('id') ? $request->id : null;
        $name = $request->has('name') ? $request->name : null;
        $email = $request->has('email') ? $request->email : null;
        $book_id = $request->has('book_id') ? $request->book_id : null;

        $users_query = User::query();
        if($id)
            $users_query->whereId($id);
        if($name)
            $users_query->whereName($name);
        if($email)
            $users_query->whereDescription($email);

        $limit = 10;
        $totalTabs = ceil($users_query->count()/$limit);

        $index = $request->has('index') ? ($request->index > $totalTabs ? $totalTabs : $request->index) : 1;
        $offset = ($index-1)*$limit;

        $users = $users_query->take($limit)->skip($offset)->get();

        $formatted = '';
        foreach ($users as $user) {
            $formatted .= '<tr>';
            $formatted .= ' <td>'.$user->id.'</td>';
            $formatted .= ' <td>'.$user->name.'</td>';
            $formatted .= ' <td>'.$user->email.'</td>';
            $formatted .= ' <td>'.$user->book_count.'</td>';
            $formatted .= ' <td>';
            if($book_id){
                $formatted .= '     <a href="'.route('books.borrow',$book_id).'" class="lend_book" data-id="'.$user->id.'"><span class="glyphicon glyphicon-ok" aria-hidden="true" title="Lend Book"></span></a>';
            }else{
                $formatted .= '     <a href="'.route('users.show',$user->id).'"><span class="glyphicon glyphicon-info-sign" aria-hidden="true" title="Info"></span></a>';
                $formatted .= '     <a href="'.route('users.edit',$user->id).'"><span class="glyphicon glyphicon-edit" aria-hidden="true" title="Edit"></span></a>';
                $formatted .= '     <a href="'.route('users.destroy',$user->id).'" class="destroy_button"><span class="glyphicon glyphicon-remove red" aria-hidden="true" title="Remove"></span></a>';
            }
            $formatted .= ' </td>';
            $formatted .= '</tr>';
        }

        $controls = PaginationHelper::getPaginationControls($index, $totalTabs);

        return response()->json(['success' => true, 'data' => $formatted, 'controls' => $controls])->header('Content-Type', 'application/json');
    }
}
