<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    private $validatorRules = [
        'name' => 'required|min:3|max:255',
        'description' => 'required|min:3|max:255'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->validatorRules);
        $category = Category::create([
            'name' => $request->name,
            'description' => $request->description
            ]);
        if($category){
            return redirect()->route('categories.show', ['category'=>$category->id]);
        }else{
            return redirect()->route('categories.create', ['errors' => ['unespecified_error' => 'there was a problem storing the category']]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id)->load('books');
        if($category)
            return view('categories.show', ['category' => $category]);
        else
            return redirect()->route('categories.index', ['error' => 'The specified category could not be found']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        if($category)
            return view('categories.edit', ['category' => $category]);
        else
            return redirect()->route('categories.index', ['error' => 'The specified category could not be found']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->validatorRules);
        $category = Category::find($id);
        if($category){
            $category->name = $request->name;
            $category->description = $request->description;
            $category->save();
            return redirect()->route('categories.show', ['category'=>$category->id]);
        }
        else
            return redirect()->route('categories.index', ['error' => 'The specified category could not be found']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        if($category){
            if($category->delete())
                $json=['success' => true, 'msg' => 'the category was deleted'];
            else
                $json=['success' => flase, 'msg' => 'the category cold not be deleted'];
            return response()->json($json)->header('Content-Type', 'application/json');
        }
        else
            return response()->json(['success' => false, 'msg' => 'the specified category could not be found'])->header('Content-Type', 'application/json');
    }

    public function data(Request $request){
        $id = $request->has('id') ? $request->id : null;
        $name = $request->has('name') ? $request->name : null;
        $description = $request->has('description') ? $request->description : null;

        $categories_query = Category::query();
        if($id)
            $categories_query->whereId($id);
        if($name)
            $categories_query->whereName($name);
        if($description)
            $categories_query->whereDescription($description);

        $limit = 10;
        $totalTabs = ceil($categories_query->count()/$limit);

        $index = $request->has('index') ? ($request->index > $totalTabs ? $totalTabs : $request->index) : 1;
        $offset = ($index-1)*$limit;

        $categories = $categories_query->take($limit)->skip($offset)->get();

        $formatted = '';
        foreach ($categories as $category) {
            $formatted .= '<tr>';
            $formatted .= ' <td>'.$category->id.'</td>';
            $formatted .= ' <td>'.$category->name.'</td>';
            $formatted .= ' <td>'.$category->description.'</td>';
            $formatted .= ' <td>'.$category->book_count.'</td>';
            $formatted .= ' <td>';
            $formatted .= '     <a href="'.route('categories.show',$category->id).'"><span class="glyphicon glyphicon-info-sign" aria-hidden="true" title="Info"></span></a>';
            $formatted .= '     <a href="'.route('categories.edit',$category->id).'"><span class="glyphicon glyphicon-edit" aria-hidden="true" title="Edit"></span></a>';
            $formatted .= '     <a href="'.route('categories.destroy',$category->id).'" class="destroy_button"><span class="glyphicon glyphicon-remove red" aria-hidden="true" title="Remove"></span></a>';
            $formatted .= ' </td>';
            $formatted .= '</tr>';
        }

        $controls = PaginationHelper::getPaginationControls($index, $totalTabs);

        return response()->json(['success' => true, 'data' => $formatted, 'controls' => $controls])->header('Content-Type', 'application/json');
    }
}
