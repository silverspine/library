<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
	use SoftDeletes;
	
    protected $fillable = ['name', 'description'];

    protected $dates =['created_at', 'updated_at', 'deleted_at'];

    function books(){
    	return $this->hasMany('App\Book');
    }

    function getBookCountAttribute(){
    	return $this->books()->count();
    }
}
