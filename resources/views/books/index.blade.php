@extends('layouts.general_index')

@section('title', 'Books')
@section('create_route', route('books.create'))

@section('table_head')
	<td><input type="number" name="id" id="id" placeholder="ID"></td>
	<td><input type="text" name="name" id="name" placeholder="Name"></td>
	<td><input type="text" name="author" id="author" placeholder="Author"></td>
	<td><input type="text" name="category" id="category" placeholder="Category" list="categories">
		<datalist id="categories">
			@foreach($categories as $category)
				<option value="{{ $category->name }}">
			@endforeach
		</datalist>
	</td>
	<td><input type="text" name="published_date" id="published_date" placeholder="Published Date"></td>
	<td><input type="text" name="borrowed" placeholder="Borrowed?" disabled></td>
	<td><input type="text" name="actions" placeholder="Actions" disabled></td>
@endsection

@section('data_url', route('books.data'))

@section('filter_data')
	id: $('#id').val(),
	name: $('#name').val(),
	author: $('#author').val(),
	category: $('#category').val(),
	published_date: $('#published_date').val(),
@endsection