@extends('layouts.default')

@section('title', 'Book Edit')

@section('content')
	<h1>
        Book Edit
    </h1>
    
    @if (count($errors) > 0)
	    <div class="alert alert-danger">
	    	<ul>
			    @foreach($errors->all() as $error)
			    	<li>{{ $error }}</li>
			    @endforeach
	    	</ul>
	    </div>
    @endif

	<div class="col-md-12 text-center">
		<form id="new_book" action="{{ route('books.update', $book->id) }}" method="post">
			{{ csrf_field() }}
			<input type="hidden" name="_method" value="PATCH">
			<div class="form-group">
				<label for="name">Name:</label>
				<input type="text" name="name" id="name" placeholder="Name" pattern=".{3,}" required title="3 characters minimum" value="{{ $book->name }}">
			</div>
			<div class="form-group">
				<label for="author">Author:</label>
				<input type="text" name="author" id="author" placeholder="Author" pattern=".{3,}" required title="3 characters minimum" value="{{ $book->author }}">
			</div>
			<div class="form-group">
				<label for="category_id">Category:</label>
				<select name="category_id" id="category_id" required>
					@foreach($categories as $category)
						<option value="{{ $category->id }}" {!! $category->id == $book->category_id ? "select" : "" !!}>{{ $category->name }}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<label for="published_date">Published Date:</label>
				<input type="date" name="published_date" id="published_date" max="{{ date('Y-m-d') }}" value="{{ $book->published_date }}">
			</div>
			<div class="form-group">
				<input type="submit" name="update_book" id="update_book" class="btn btn-primary">
			</div>
		</form>
	</div>
@endsection