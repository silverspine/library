@extends('layouts.pagination_script')

@section('title', 'Book View')

@section('header_content')
	<h1>
        Book View <a href="{{ route('books.edit', $book->id) }}" class="pull_right"><span class="glyphicon glyphicon-edit" aria-hidden="true" title="Edit Book"></a>
    </h1>
	<div class="col-md-12">
		<table>
			<tr>
				<td>ID</td><td>{{ $book->id }}</td>
			</tr>
			<tr>
				<td>Name</td><td>{{ $book->name }}</td>
			</tr>
			<tr>
				<td>Author</td><td>{{ $book->author }}</td>
			</tr>
			<tr>
				<td>Category</td><td><a href="{{ route('categories.show', $book->category->id) }}">{{ $book->category->name }}</td>
			</tr>
			<tr>
				<td>Published Date</td><td>{{ $book->published_date }}</td>
			</tr>
			@if($book->borrowed)
				<tr>
					<td>Borrowed By</td><td><a href="{{ route('users.show', $book->user->id) }}">{{ $book->user->name }}</a></td>
				</tr>
			@endif
		</table>
		@if(!$book->borrowed)
			<div class="text-center">
				<button class="lend_book btn btn-primary" data-toggle="modal" data-target="#lendBookModal" data-url="{{ route('books.borrow',$book->id) }}">Lend Book</button>
			</div>
		@endif
	</div>
	<div class="modal fade" id="lendBookModal" tabindex="-1" role="dialog" aria-labelledby="lendBookLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="lendBookLabel">Return Book</h4>
				</div>
				<div class="modal-body">
					<p>Select a user to lend the book</p>
@endsection

@section('table_head')
	<td><input type="number" name="id" id="id" placeholder="ID"></td>
	<td><input type="text" name="name" id="name" placeholder="Name"></td>
	<td><input type="email" name="email" id="email" placeholder="e-mail"></td>
	<td><input type="text" name="book_count" id="book_count" placeholder="Borrowed Books" disabled></td>
	<td><input type="text" name="actions" placeholder="Actions" disabled></td>
@endsection

@section('other_content')
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('data_url', route('users.data'))

@section('filter_data')
	id: $('#id').val(),
	name: $('#name').val(),
	email: $('#email').val(),
	book_id: "{{ $book->id }}",
@endsection

@section('custom_scripts')
	$('#lendBookModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var modal = $(this);
	})

	$('body').on('click', 'a.lend_book', function(e) {
        e.preventDefault();
        var route = $(this).attr('href');
        var user_id = $(this).data('id');
        $.ajax({
            type: "POST",
            url : route,
            data: {
                _token: "{{ csrf_token() }}",
            	user_id: user_id,
            },
        }).done(function (result) {
            window.location.href = "{{ route('books.show', $book->id) }}";
        }).fail(function () {
            alert('The data could not be loaded.');
        });
        $('#lendBookModal').modal('hide');
    });
@endsection