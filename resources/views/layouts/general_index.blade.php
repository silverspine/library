@extends('layouts.pagination_script')

@section('header_content')
    <h1>
        @yield('title') 
        <a href="@yield('create_route')" class="create_new_entry pull_right"><span class="glyphicon glyphicon-plus" aria-hidden="true" title="Create new entry"></a>
        <a href="#" class="refresh_and_filter pull_right"><span class="glyphicon glyphicon-refresh" aria-hidden="true" title="Refresh and Filter"></a>
        <a href="#" class="clear_filters pull_right"><span class="glyphicon glyphicon-remove" aria-hidden="true" title="Clear Filter Fields"></a>
    </h1>
@endsection

@section('custom_scripts')
    $('body').on('click', 'a.destroy_button', function(e) {
        e.preventDefault();
        var selection = confirm("Are you shure you want to destroy the element?");
        var route = $(this).attr('href');
        if(selection == true){
            $.ajax({
                type: "POST",
                url : route,
                data: {_method: "DELETE", _token: "{{ csrf_token() }}"},
            }).done(function (result) {
                getData($('.pagination a.active').data('val'))
            }).fail(function () {
                alert('The elemetn could not be deleted.');
            });
        }
    });
@endsection