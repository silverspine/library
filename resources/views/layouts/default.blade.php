<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Library - @yield('title')</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="{{ asset('css/library.css') }}">
    </head>
    <body>
        <header class="title m-b-md">
            Library App
        </header>
        <div id="navigation text-center">
            <ul class="nav nav-tabs nav-justified">
                <li role="presentation"{!! (Request::is('books') || Request::is('books/*') ? ' class="active"' : '') !!}><a href="{{ route('books.index') }}">Books</a></li>
                <li role="presentation"{!! (Request::is('categories') || Request::is('categories/*') ? ' class="active"' : '') !!}><a href="{{ route('categories.index') }}">Categories</a></li>
                <li role="presentation"{!! (Request::is('users') || Request::is('users/*') ? ' class="active"' : '') !!}><a href="{{ route('users.index') }}">Users</a></li>
            </ul>
        </div>

        <div class="container">
            @yield('content')
        </div>
        @yield('footer_scripts')
    </body>
</html>