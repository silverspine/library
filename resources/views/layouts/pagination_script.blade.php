@extends('layouts.default')

@section('content')
    @yield('header_content')
    <div id="data-table" class="col.md-6 text-center">
        <table>
            <thead>
                @yield('table_head')
            </thead>
            <tbody>
                
            </tbody>
        </table>
        <div class="pagination text-center">
            
        </div>
    </div>
    @yield('other_content')
@endsection

@section('footer_scripts')
<script>
    $( document ).ready(function() {
        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
        });

        getData(1);

        $('body').on('click', 'a.refresh_and_filter', function(e) {
            e.preventDefault();
            var route = $(this).attr('href');
            getData(1)
        });

        $('body').on('click', 'a.clear_filters', function(e) {
            e.preventDefault();
            $('input').val("");
            getData(1);
        });

        $('body').on('click', '.pagination a', function(e) {
            e.preventDefault();
            if(!$(this).hasClass('dissabled'))
                getData($(this).data('val'));
        });

        function getData(index) {
            $.ajax({
                type: "POST",
                url : "@yield('data_url')",
                data: {
                    index: index,
                    _token: "{{ csrf_token() }}",
                    @yield('filter_data')
                },
            }).done(function (result) {
                $('#data-table tbody').html(result.data);
                $('.pagination').html(result.controls);
            }).fail(function () {
                alert('The data could not be loaded.');
            });
        }

        @yield('custom_scripts')
    });
</script>
@endsection