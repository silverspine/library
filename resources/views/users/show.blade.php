@extends('layouts.pagination_script')

@section('title', 'User View')

@section('header_content')
	<h1>
        User View <a href="{{ route('users.edit', $user->id) }}" class="pull_right"><span class="glyphicon glyphicon-edit" aria-hidden="true" title="Edit User"></a>
    </h1>
	<div class="col-md-12">
		<table>
			<tr>
				<td>ID</td><td>{{ $user->id }}</td>
			</tr>
			<tr>
				<td>Name</td><td>{{ $user->name }}</td>
			</tr>
			<tr>
				<td>e-mail</td><td>{{ $user->email }}</td>
			</tr>
		</table>
	</div>
	<h2>Books borrowed by this User</h2>
@endsection

@section('table_head')
	<td><input type="number" name="id" id="id" placeholder="ID"></td>
	<td><input type="text" name="name" id="name" placeholder="Name"></td>
	<td><input type="text" name="author" id="author" placeholder="Author"></td>
	<td><input type="text" name="category" id="category" placeholder="Category"></td>
	<td><input type="text" name="published_date" id="published_date" placeholder="Published Date"></td>
	<td><input type="text" name="actions" placeholder="Actions" disabled></td>
@endsection

@section('other_content')
	<div class="modal fade" id="returnBookModal" tabindex="-1" role="dialog" aria-labelledby="returnBookLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="returnBookLabel">Return Book</h4>
				</div>
				<div class="modal-body">
					<p>Are you shure you want to return this book?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" href="#" class="btn btn-primary">OK</button>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('data_url', route('books.data'))

@section('filter_data')
	user_id: {{$user->id}},
	id: $('#id').val(),
	name: $('#name').val(),
	author: $('#author').val(),
	published_date: $('#published_date').val(),
@endsection

@section('custom_scripts')
	$('#returnBookModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var url = button.data('url');
		var modal = $(this);
		modal.find('.btn-primary').attr('href', url);
	})

	$('body').on('click', '.modal-footer > button.btn-primary', function(e) {
        e.preventDefault();
        var route = $(this).attr('href');
        $.ajax({
            type: "POST",
            url : route,
            data: {
                _token: "{{ csrf_token() }}"
            },
        }).done(function (result) {
            getData($('a.active').data('val'));
        }).fail(function () {
            alert('The data could not be loaded.');
        });
        $('#returnBookModal').modal('hide');
    });
@endsection