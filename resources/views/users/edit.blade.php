@extends('layouts.default')

@section('title', 'User Edit')

@section('content')
	<h1>
        User Edit
    </h1>
    
    @if (count($errors) > 0)
	    <div class="alert alert-danger">
	    	<ul>
			    @foreach($errors->all() as $error)
			    	<li>{{ $error }}</li>
			    @endforeach
	    	</ul>
	    </div>
    @endif

	<div class="col-md-12 text-center">
		<form id="new_user" action="{{ route('users.update', $user->id) }}" method="post">
			{{ csrf_field() }}
			<input type="hidden" name="_method" value="PATCH">
			<div class="form-group">
				<label for="name">Name:</label>
				<input type="text" name="name" id="name" placeholder="Name" pattern=".{3,}" required title="3 characters minimum" value="{{ $user->name }}">
			</div>
			<div class="form-group">
				<label for="email">e-mail:</label>
				<input type="email" name="email" id="email" placeholder="e-mail" pattern=".{3,}" required title="3 characters minimum" value="{{ $user->email }}">
			</div>
			<div class="form-group">
				<input type="submit" name="update_user" id="update_user" class="btn btn-primary">
			</div>
		</form>
	</div>
@endsection