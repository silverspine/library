@extends('layouts.default')

@section('title', 'User Creation')

@section('content')
	<h1>
        User Create
    </h1>
    
    @if (count($errors) > 0)
	    <div class="alert alert-danger">
	    	<ul>
			    @foreach($errors->all() as $error)
			    	<li>{{ $error }}</li>
			    @endforeach
	    	</ul>
	    </div>
    @endif

	<div class="col-md-12 text-center">
		<form id="new_user" action="{{ route('users.store') }}" method="post">
			{{ csrf_field() }}
			<div class="form-group">
				<label for="name">Name:</label>
				<input type="text" name="name" id="name" placeholder="Name" pattern=".{3,}" required title="3 characters minimum">
			</div>
			<div class="form-group">
				<label for="email">e-mail:</label>
				<input type="email" name="email" id="email" placeholder="e-mail" pattern=".{3,}" required title="3 characters minimum">
			</div>
			<div class="form-group">
				<input type="submit" name="create_user" id="create_user" class="btn btn-primary">
			</div>
		</form>
	</div>
@endsection