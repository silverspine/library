@extends('layouts.general_index')

@section('title', 'Categories')
@section('create_route', route('users.create'))

@section('table_head')
	<td><input type="number" name="id" id="id" placeholder="ID"></td>
	<td><input type="text" name="name" id="name" placeholder="Name"></td>
	<td><input type="email" name="email" id="email" placeholder="e-mail"></td>
	<td><input type="text" name="book_count" id="book_count" placeholder="Borrowed Books" disabled></td>
	<td><input type="text" name="actions" placeholder="Actions" disabled></td>
@endsection

@section('data_url', route('users.data'))

@section('filter_data')
	id: $('#id').val(),
	name: $('#name').val(),
	email: $('#email').val(),
@endsection