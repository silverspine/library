@extends('layouts.default')

@section('title', 'Category Edit')

@section('content')
	<h1>
        Category Edit
    </h1>
    
    @if (count($errors) > 0)
	    <div class="alert alert-danger">
	    	<ul>
			    @foreach($errors->all() as $error)
			    	<li>{{ $error }}</li>
			    @endforeach
	    	</ul>
	    </div>
    @endif

	<div class="col-md-12 text-center">
		<form id="new_category" action="{{ route('categories.update', $category->id) }}" method="post">
			{{ csrf_field() }}
			<input type="hidden" name="_method" value="PATCH">
			<div class="form-group">
				<label for="name">Name:</label>
				<input type="text" name="name" id="name" placeholder="Name" pattern=".{3,}" required title="3 characters minimum" value="{{ $category->name }}">
			</div>
			<div class="form-group">
				<label for="description">Description:</label>
				<textarea name="description" id="description" placeholder="Description" pattern=".{3,}" required title="3 characters minimum">{{ $category->description }}</textarea>
			</div>
			<div class="form-group">
				<input type="submit" name="update_category" id="update_category" class="btn btn-primary">
			</div>
		</form>
	</div>
@endsection