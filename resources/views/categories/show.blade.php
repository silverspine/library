@extends('layouts.pagination_script')

@section('title', 'Category View')

@section('header_content')
	<h1>
        Category View <a href="{{ route('categories.edit', $category->id) }}" class="pull_right"><span class="glyphicon glyphicon-edit" aria-hidden="true" title="Edit Category"></a>
    </h1>
	<div class="col-md-12">
		<table>
			<tr>
				<td>ID</td><td>{{ $category->id }}</td>
			</tr>
			<tr>
				<td>Name</td><td>{{ $category->name }}</td>
			</tr>
			<tr>
				<td>Description</td><td>{{ $category->description }}</td>
			</tr>
		</table>
	</div>
	<h2>Books with this Category</h2>
@endsection

@section('table_head')
	<td><input type="number" name="id" id="id" placeholder="ID"></td>
	<td><input type="text" name="name" id="name" placeholder="Name"></td>
	<td><input type="text" name="author" id="author" placeholder="Author"></td>
	<td><input type="text" name="published_date" id="published_date" placeholder="Published Date"></td>
	<td><input type="text" name="borrowed" placeholder="Borrowed?" disabled></td>
	<td><input type="text" name="actions" placeholder="Actions" disabled></td>
@endsection

@section('data_url', route('books.data'))

@section('filter_data')
	category_id: {{$category->id}},
	id: $('#id').val(),
	name: $('#name').val(),
	author: $('#author').val(),
	published_date: $('#published_date').val(),
@endsection