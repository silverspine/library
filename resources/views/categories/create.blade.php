@extends('layouts.default')

@section('title', 'Category Creation')

@section('content')
	<h1>
        Category Create
    </h1>
    
    @if (count($errors) > 0)
	    <div class="alert alert-danger">
	    	<ul>
			    @foreach($errors->all() as $error)
			    	<li>{{ $error }}</li>
			    @endforeach
	    	</ul>
	    </div>
    @endif

	<div class="col-md-12 text-center">
		<form id="new_category" action="{{ route('categories.store') }}" method="post">
			{{ csrf_field() }}
			<div class="form-group">
				<label for="name">Name:</label>
				<input type="text" name="name" id="name" placeholder="Name" pattern=".{3,}" required title="3 characters minimum">
			</div>
			<div class="form-group">
				<label for="description">Description:</label>
				<textarea name="description" id="description" placeholder="Description" pattern=".{3,}" required title="3 characters minimum"></textarea>
			</div>
			<div class="form-group">
				<input type="submit" name="create_category" id="create_category" class="btn btn-primary">
			</div>
		</form>
	</div>
@endsection