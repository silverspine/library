<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail
    ];
});

$factory->define(App\Category::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->word,
        'description' => $faker->sentence
    ];
});

$factory->define(App\Book::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->word,
        'author' => $faker->name,
        'category_id' => App\Category::inRandomOrder()->first()->id,
        'published_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'user_id' => rand(0,1) ? App\User::inRandomOrder()->first()->id : null
    ];
});