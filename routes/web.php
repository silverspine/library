<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('books.index');
});

Route::post('books/{book}/borrow', 'BookController@borrow')->name('books.borrow');
Route::post('books/{book}/retrieve', 'BookController@retrieve')->name('books.retrieve');
Route::post('books/data', 'BookController@data')->name('books.data');
Route::resource('books', 'BookController');

Route::post('categories/data', 'CategoryController@data')->name('categories.data');
Route::resource('categories', 'CategoryController');

Route::post('users/data', 'UserController@data')->name('users.data');
Route::resource('users', 'UserController');
